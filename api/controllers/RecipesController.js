// noinspection JSUnusedGlobalSymbols
/**
 * RecipesController
 *
 * @description :: Server-side logic for managing recipes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	/**
   * `RecipesController.create()`
   */
  create: async function (req, res) {
    const { name, servings, description, source, ingredients, mealPrep, instructions } = req.body;

    try {
      const createdRecipe = await recipes.createNewRecipe({
        name,
        servings,
        description,
        source,
        ingredients,
        mealPrep,
        instructions
      });

      return res.ok(createdRecipe);
    } catch(e) {
      sails.log(e);
      return res.badRequest(e);
    }
  },


  /**
   * `RecipesController.findOne()`
   */
  findOne: async function (req, res) {
    const url = req.param('recipe');

    try {
      const recipeData = await recipes.findRecipe(url);

      // If the recipe doesn't return any data, it doesn't exist.
      if (! recipeData) return res.notFound();

      return res.ok(recipeData);
    } catch(e) {
      return res.serverError(e);
    }
  },


  /**
   * `RecipesController.findAll()`
   */
  findAll: async function (req, res) {
    const allRecipes = await recipes.findAllRecipes();

    if (! allRecipes) return res.notFound();
    return res.ok(allRecipes);
  },


  /**
   * `RecipesController.update()`
   */
  update: function (req, res) {
    let url = req.param('recipe');

    let name = req.param('name') || null;
    let description = req.param('description') || null;
    let image = req.param('image') || null;
    recipes
      .update({url}, {name, url, description})
      .then(recipe => {
        return res.ok(recipe);
      })
      .catch(err => {
        return res.serverError(err);
      });
  },


  /**
   * `RecipesController.delete()`
   */
  delete: function (req, res) {
    return res.json({
      todo: 'delete() is not implemented yet!'
    });
  },

  /**
   * Get a de-duplicated array of all ingredients in the database.
   *
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  getIngredients: async (req, res) => {
    try {
      const allIngredients = await ingredients.find({ select: 'name', sort: 'name ASC' });

      // Create a pure array of names.
      const processedIngredients = allIngredients.map(ingredient => ingredient.name);

      // Return a de-duplicated array of all ingredients.
      return res.ok(Array.from(new Set(processedIngredients)));
    } catch(e) {
      sails.log(e);
      return res.serverError(e);
    }
  },

  /**
   * Get a de-duplicated array of all sources in the database.
   *
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  getSources: async (req, res) => {
    try {
      // Get all non-empty sources.
      const sources = await recipes.find({ select: 'source', where: { source: {'!=': ''} }, sort: 'source ASC'});

      // Create a pure array of names.
      const processedSources = sources.map(v => v.source);

      // Return a de-duplicated array of all ingredients.
      return res.ok(Array.from(new Set(processedSources)));
    } catch(e) {
      sails.log(e);
      return res.serverError(e);
    }
  }
};

