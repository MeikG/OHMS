/**
 * users.js
 *
 * @description :: Users table.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
let bcrypt = require('bcrypt');

module.exports = {
  primaryKey: 'id',

  attributes: {
    username: {
      type: 'string',
      unique: 'true'
    },

    password: {
      type: 'string'
    }
  },

  /**
   * Before creating a new user account, hash the password and store it.
   *
   * @param values
   * @param cb
   */
  beforeCreate: (values, cb) => {
    bcrypt.hash(values.password, 10, function(err, hash) {
      if(err) return cb(err);
      values.password = hash;
      cb();
    });
  }
};

