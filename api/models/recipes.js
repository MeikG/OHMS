// noinspection JSUnusedGlobalSymbols
/**
 * recipes.js
 *
 * @description :: Recipes
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  primaryKey: 'id',

  attributes: {
    // Recipe name && web safe url.
    name: {
      type: 'string',
      required: true,
      minLength: 4,
      maxLength: 140,
      regex: /^[A-Z0-9]/
    },
    url:  {
      type: 'string',
      unique: true
    },

    servings: {
      type: 'number',
      required: true,
      min: 1,
      max: 100
    },

    // Recipe Description.
    description: {
      type: 'string'
    },

    source: {
      type: 'string'
    },

    // Recipe main image.
    primaryImage: {
      type: 'string'
    },

    ingredients: {
      collection: 'ingredients',
      via: 'recipe'
    },

    mealPrep: {
      collection: 'mealPrep',
      via: 'recipe'
    },

    instructions: {
      collection: 'instructions',
      via: 'recipe'
    }
  },

  validation: [
    'ok',
    'notFound',
    'serverError'
  ],

  /**
   * Generate a web safe URL slug.
   *
   * @param   {string} text   String to convert to slug
   * @returns {string}
   * @private
   */
  _generateSlug(text) {
    let slug = Math.random().toString(36).substr(2, 5).toLowerCase();

    return text.toString().toLowerCase()
      .replace(/\s+/g, '-')       // Replace spaces with -
      .replace(/[^\w\-]+/g, '')   // Remove all non-word chars
      .replace(/\-\-+/g, '-')     // Replace multiple - with single -
      .replace(/^-+/, '')         // Trim - from start of text
      .replace(/-+$/, '')         // Trim - from end of text
      .concat(`-${slug}`);        // Append a random slug to the end
  },

  /**
   * Automatically generate a URL safe slug.
   *
   * @param {object} values
   * @param cb
   */
  beforeCreate: (values, cb) => {
    // Encode the URL
    values.url = recipes._generateSlug(values.name);
    return cb();
  },

  afterDestroy: async ({ id }, cb) => {
    // If ingredients have been created, then remove them here.
    await ingredients.destroy({ recipe: id });

    // Remove all of the meal prep steps.
    await mealPrep.destroy({ recipe: id });

    // Remove all of the recipe instructions.
    await instructions.destroy({ recipe: id });

    return cb();
  },

  async createNewRecipe(data) {
    let id;

    // Fail if the ingredients isn't an array, or empty.
    if (! Array.isArray(data.ingredients) || data.ingredients.length < 1) {
      throw ('Ingredients missing or empty');
    }

    // Fail if the meal prep isn't an array.
    if (! Array.isArray(data.mealPrep)) {
      throw ('Meal Prep steps are missing');
    }

    // Fail if the instructions isn't an array, or empty.
    if (! Array.isArray(data.instructions) || data.instructions.length < 1) {
      throw ('Instructions missing or empty');
    }

    try {
      const { name, servings, description, source } = data;

      // Create the recipe, and grab the ID to be used later.
      const recipe = await this
        .create({
          name,
          servings,
          description,
          source
        }).fetch();

      id = recipe.id;

      // Filter out empty records.
      const filteredIngredients = data.ingredients || []
        .filter(request => request.quantity.length > 0 && request.name.length > 0);

      const filteredMealPrep = data.mealPrep || []
        .filter(instruction => instruction.timing !== null && instruction.step.length > 0);

      const filteredInstructions = data.instructions || []
        .filter(instruction => instruction.timing !== null && instruction.step.length > 0);

      // Create the ingredients, meal prep and instruction records.
      const generatedIngredients = await ingredients
        .createEach(filteredIngredients)
        .fetch();

      const generatedMealPrep = await mealPrep
        .createEach(filteredMealPrep)
        .fetch();

      const generatedInstructions = await instructions
        .createEach(filteredInstructions)
        .fetch();

      // Attach the ingredients, meal prep and instructions to the recipe.
      await this
        .addToCollection(id, 'ingredients')
        .members(generatedIngredients.map(ingredient => ingredient.id));

      await this
        .addToCollection(id, 'mealPrep')
        .members(generatedMealPrep.map(instruction => instruction.id));

      await this
        .addToCollection(id, 'instructions')
        .members(generatedInstructions.map(instruction => instruction.id));

      return this.findRecipe(recipe.url);
    } catch(e) {
      // If any of the above steps fail, delete the recipe (which deletes all associated records).
      await this.destroy({ id });

      // Throw the error again so it can be caught at the controller.
      throw e;
    }
  },

  /**
   * Finds all recipes.
   *
   * @returns {Promise}
   */
  findAllRecipes() {
    return new Promise((resolve, reject) => {
      recipes
        .find()
        .exec((err, recipes) => {
          if (err) return reject(err);
          let mappedRecipes = recipes.map(recipe => {
            recipe.url = `/recipes/${recipe.url}`;
            return recipe;
          });

          return resolve(mappedRecipes);
        });
    });
  },

  async findRecipe(url) {
    return await this
      .findOne({ url })
      .populate('ingredients')
      .populate('mealPrep')
      .populate('instructions')
  }
};

