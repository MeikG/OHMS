module.exports = {

  primaryKey: 'id',

  attributes: {

    recipe: {
      model: 'recipes'
    },

    timing: {
      type: 'number',
      allowNull: true,
      min: 1,
      max: 999
    },
    step: {
      type: 'string',
      required: true,
      minLength: 10,
      regex: /^[A-Z0-9]/
    },

    order: {
      type: 'number',
      required: true,
      min: 0,
      max: 999
    }
  }
};
