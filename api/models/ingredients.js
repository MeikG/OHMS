module.exports = {

  primaryKey: 'id',

  attributes: {

    recipe: {
      model: 'recipes'
    },

    quantity: {
      type: 'string',
      required: true,
      minLength: 1,
      maxLength: 100,
      regex: /^([0-9\/\.]{1,}) ?((m|k)?g|oz|lb|(m|c)?m|in|m?l|tb?sp|cups?|pi?nts?|quarts?|qts?|gal(lons)?|p?inch|p?inches|slices?|dash|dashes)?$/
    },
    name: {
      type: 'string',
      required: true,
      minLength: 1,
      maxLength: 140,
      regex: /^[A-Z0-9]/
    },

    order: {
      type: 'number',
      required: true,
      min: 0,
      max: 999
    }
  }
};
