/**
 * versionCheck
 *
 * @module      :: Policy
 * @description :: Ensures that requests from clients match the current server version
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {

  // Ignore non-JSON GET requests.
  if (! req.wantsJSON && req.method === 'GET') return next();

  // If the Client-Version header is not set, respond with bad request.
  if (typeof req.headers['version'] === 'undefined') return res.badRequest({'error': 'Missing Version header'});

  // Only proceed if the client matches the server version.
  if (req.headers['version'] === sails.config.site.version) return next();

  // Client version mismatch.
  return res.badRequest({'error': 'Incompatible Client-Version, please update.'});
};
