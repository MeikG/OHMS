/**
 * singlePageApp
 *
 * @module      :: Policy
 * @description :: Simple policy to halt any request that doesn't want JSON, and respond with our single page app entry-point
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
const path = require('path');

module.exports = function(req, res, next) {

  // If the request wants JSON then allow the request to proceed.
  if (req.wantsJSON) return next();

  // Send the SPA to the user.
  return res
    .status(200)
    .sendFile(`${path.resolve(__dirname, '../../dist/index.html')}`);
};
