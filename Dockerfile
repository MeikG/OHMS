FROM node:8.2.1
MAINTAINER Mike Gregory <meikura@gmail.com>

COPY . /app
ENV NODE_ENV production

WORKDIR /app
CMD npm run production
