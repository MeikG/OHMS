const webpack = require('webpack');
const CompressionPlugin = require("compression-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const OfflinePlugin = require('offline-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const Visualizer = require('webpack-visualizer-plugin');

module.exports = env => {
  let options = {
    dev: env.NODE_ENV !== 'production',
  };

  // Set the version to either a random dev string, or a variable passed in by CI.
  options.version = options.dev ? 'developer' : env.OHMS_VERSION;

  return {
    entry: {
      app: './frontend/index.jsx',
    },
    output: {
      path: __dirname + '/dist',
      publicPath: '/',
      filename: options.dev ? '[name].js' : '[name].[chunkhash].js',
      chunkFilename: options.dev ? '[name].js' : '[name].[chunkhash].js',
    },
    plugins: [
      new Visualizer(),

      new webpack.DefinePlugin({ // <-- key to reducing React's size
        'process.env': {
          'NODE_ENV': JSON.stringify('production'),
          'OHMS_VERSION': JSON.stringify(options.version)
        }
      }),
      new webpack.optimize.AggressiveMergingPlugin(),

      // Generate a chunk for the 3rd party libraries that will likely not change.
      new webpack.optimize.CommonsChunkPlugin({
        name: "vendor",
        minChunks: function (module) {
          // this assumes your vendor imports exist in the node_modules directory
          return module.context && module.context.indexOf("node_modules") !== -1;
        }
      }),

     // Generate a manifest chunk.
      new webpack.optimize.CommonsChunkPlugin({
        name: 'manifest',
        minChunks: Infinity
      }),

      new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.(js)$/,
        threshold: 10240,
        minRatio: 0.0
      }),

      new WebpackPwaManifest({
        name: 'PocketSous',
        short_name: 'PocketSous',
        description: 'The Sous Chef in your Pocket',
        version: options.version,
        start_url: ".",
        display: "standalone",
        lang: "en",
        background_color: '#00695c',
        icons: []
      }),

      new HtmlWebpackPlugin({
        version: options.version,
        inject: 'head',
        template: 'frontend/index.html',
        chunks: ['manifest', 'vendor', 'common', 'app']
      }),

      new OfflinePlugin({
        publicPath: '/',
        externals: [
          '/'
        ],
        version: () => options.version,
        AppCache: {
          FALLBACK: { '/': '/' }
        },
        ServiceWorker: {
          events: true,
          navigateFallbackURL: '/'
        },
      }),
    ],
    module: {
      rules: [
        {
          test: /\.svg/,
          use: {
            loader: "file-loader?limit=65000&mimetype=image/svg+xml&name=images/[name].[ext]"
          }
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: "style-loader"
            },
            {
              loader: "css-loader",
              options: {
                importLoaders: 1,
                modules: true,
                localIdentName: options.dev ? '[path]___[name]__[local]___[hash:base64:5]' : '[hash:base64:5]',
              }
            }
          ]
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: "style-loader"
            },
            {
              loader: "css-loader",
              options: {
                importLoaders: 1,
                modules: true,
                localIdentName: options.dev ? '[path]___[name]__[local]___[hash:base64:5]' : '[hash:base64:5]',
              }
            },
            {
              loader: "sass-loader"
            }
          ]
        },
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              plugins:  [
                ["react-css-modules", {
                  generateScopedName: options.dev ? '[path]___[name]__[local]___[hash:base64:5]' : '[hash:base64:5]',
                  filetypes: {
                    ".scss": {
                      "syntax": "postcss-scss"
                    }
                  },
                  handleMissingStyleName: 'warn'
                }]
                , "transform-object-rest-spread", "transform-decorators-legacy","transform-class-properties", ["transform-react-jsx", { "pragma": "h" }]],
              presets: ["stage-1", "react", "env"]
            }
          }
        },
        {
          test: /\.(ttf|woff2?)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[hash].[ext]'
              }
            }
          ]
        }
      ]
    },
    resolve: {
      extensions: [".ts", ".js", ".tsx", ".jsx", ".scss"],
      alias: {
        'react': 'preact-compat',
        'react-dom': 'preact-compat',
        'react-tap-event-plugin': 'preact-tap-event-plugin'
      }
    },
    watch: options.dev,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
      ignored: /node_modules/
    },
    devtool: 'source-map'
  };
};
