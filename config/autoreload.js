// [your-sails-app]/config/autoreload.js
module.exports.autoreload = {
  active: true,
  usePolling: true,
  overrideMigrateSetting: false,
  dirs: [
    "api/models",
    "api/controllers",
    "api/services",

  ]
};
