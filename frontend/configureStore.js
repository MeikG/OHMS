import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk';
import { offline } from 'redux-offline';
import reducers from './reducers';
import offlineConfig from 'redux-offline/lib/defaults';

export default function configureStore(preloadedState) {
  return createStore(
    reducers,
    preloadedState,
    compose(
      applyMiddleware(
        thunkMiddleware
      ),
      offline(offlineConfig)
    )
  )
}
