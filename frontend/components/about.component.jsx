import { h } from 'preact';
import typography from './styles/typography.scss';
import layout from './styles/layout.scss';

export default () => (
  <div>
    <h1 styleName="typography.display3 typography.noBottomMargin">Pocket Sous</h1>
    <p styleName="typography.title">by Mike Gregory and Lauren Morelli</p>
    <p>Version:  #{ process.env.OHMS_VERSION }</p>
  </div>
);
