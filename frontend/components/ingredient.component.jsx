import { h } from 'preact';

import { TextValidator, AutoCompleteValidator } from 'react-material-ui-form-validator';
import FlatButton from 'material-ui/FlatButton';

import Cancel from 'material-ui/svg-icons/navigation/cancel';
import flexbox from 'flexboxgrid/dist/flexboxgrid.css';

export default props => {
  if (! props.editing) {
    return (
      <li>
        <strong>{ props.quantity }</strong><span style={{ marginLeft: 15 }}>{ props.name }</span>
      </li>
    )
  } else {
    let quantityValidators = [], quantityErrorMessages = [], nameValidators = [], nameErrorMessages = [];

    // Require at least 1 quantity and ingredient.
    if (props.required && props.index === 0) {
      quantityValidators.push("minStringLength: 1");
      quantityErrorMessages.push("Required");
      nameValidators.push("minStringLength: 1");
      nameErrorMessages.push("At least 1 ingredient is required");
    }

    // Only apply validators to everything but the last entry.
    if (props.index < props.length - 1) {
      quantityValidators.push("matchRegexp:^([0-9\\/\\.]{1,}) ?((m|k)?g|oz|lb|(m|c)?m|in|m?l|tb?sp|cups?|pi?nts?|quarts?|qts?|gal(lons)?|p?inch|p?inches|slices?|dash|dashes)?$");
      quantityErrorMessages.push('Unknown quantity');
      nameValidators.push("matchRegexp:^[A-Z0-9]", "minStringLength: 1");
      nameErrorMessages.push('Ingredients should start with a number or capital letter', "Please enter an ingredient");
    }

    return (
      <li>
        <div styleName="flexbox.row" style={{ display: 'inline-flex', width: '100%', justifyContent: 'space-evenly' }}>
          <div styleName="flexbox.col-xs flexbox.col-sm" style={{ maxWidth: 85 }}>
            <TextValidator
              floatingLabelText={ `Quantity${ props.index === 0 ? '*' : '' }` }
              name={ `quantity_${ props.index }` }
              value={ props.quantity }
              onChange={ props.updateQuantity }
              fullWidth
              validators={ quantityValidators }
              errorMessages={ quantityErrorMessages }
              autocomplete="off"
            />
          </div>
          <div styleName="flexbox.col-xs flexbox.col-sm-9">
            <AutoCompleteValidator
              floatingLabelText={ `Ingredient${ props.index === 0 ? '*' : '' }` }
              name={ `ingredient_${ props.index }` }
              value={ props.name }
              searchText={ props.name }
              onFocus={ props.fetchIngredients }
              onUpdateInput={ props.updateName }
              fullWidth
              dataSource={ props.autoCompleteIngredients }
              validators={ nameValidators }
              errorMessages={ nameErrorMessages }
              autocomplete="off"
            />
          </div>
          <div styleName="flexbox.col-xs flexbox.col-sm" style={{
            paddingTop: 28,
            maxWidth: 40
          }}>
            <FlatButton
              icon={<Cancel />}
              onClick={ props.deleteIngredient }
              disabled={ props.index >= props.length - 1 }
              fullWidth
              primary
              containerElement="label"
            />
          </div>
        </div>
      </li>
    )
  }
}
