import { h } from 'preact';
import typography from './styles/typography.scss';
import layout from './styles/layout.scss';

export default () => (
  <div>
    <div styleName="layout.pane typography.text-center">
      <h1 styleName="typography.display2">Welcome to Pocket Sous</h1>
    </div>
  </div>
);
