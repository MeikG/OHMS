import { h, Component } from 'preact';
import Homepage from './homepage.component';
import About from './about.component';
import Recipes from '../containers/recipes.container';
import AddNewRecipe from '../containers/addNewRecipe.container';
import ViewIndividualRecipe from '../containers/viewIndividualRecipe.container';
import Router from 'preact-router';

export default class extends Component {
  render() {
    return (
      <Router>
        <Homepage path="/"/>
        <Recipes path="/recipes"/>
        <ViewIndividualRecipe path="/recipes/:recipe"/>
        <AddNewRecipe path="/recipes/add-new"/>
        <About path="/about"/>
      </Router>
    )
  }
}
