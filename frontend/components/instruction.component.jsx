import { h } from 'preact';

import { TextValidator } from 'react-material-ui-form-validator';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

import Cancel from 'material-ui/svg-icons/navigation/cancel';
import flexbox from 'flexboxgrid/dist/flexboxgrid.css';

export default props => {
  if (! props.editing) {
    return (
      <li>
        <p>{ props.step }</p>
        { props.timing && (
          <em style={{ marginLeft: 5 }}>- { props.timing } minute{ props.timing === 1 ? '' : 's' }.</em>
        ) }
      </li>
    )
  } else {
    let stepValidators = [], stepErrorMessages = [];

    // Require at least 1 quantity and ingredient.
    if (props.required && props.index === 0) {
      stepValidators.push("minStringLength: 1");
      stepErrorMessages.push("At least one instruction is required");
    }

    // Only apply validators to everything but the last entry.
    if (props.index < props.length - 1) {
      stepValidators.push("matchRegexp:^[A-Z0-9]", "minStringLength: 10");
      stepErrorMessages.push('Instructions should start with a number or capital letter', 'Instruction should be at least 10 characters');
    }

    return (
      <li>
        <div styleName="flexbox.row" style={{ display: 'inline-flex', width: '100%', justifyContent: 'space-between' }}>
          <div styleName="flexbox.col-xs" style={{ maxWidth: 75 }}>
            <TextField
              floatingLabelText="Minutes"
              name={ `timing_${ props.index }` }
              type="number"
              min={1}
              max={999}
              value={ props.timing }
              onChange={ props.updateTiming }
              fullWidth
              autocomplete="off"
            />
          </div>
          <div styleName="flexbox.col-xs flexbox.col-sm-10">
            <TextValidator
              floatingLabelText={ `Steps${ props.required && props.index === 0 ? '*' : '' }` }
              name={ `${ props.uniqueName }_${ props.index }` }
              value={ props.step }
              onChange={ props.updateStep }
              fullWidth
              validators={ stepValidators }
              errorMessages={ stepErrorMessages }
              multiLine
              rows={ 5 }
              rowsMax={ 5 }
              autocomplete="off"
            />
          </div>
          <div styleName="flexbox.col-xs flexbox.col-sm" style={{
            paddingTop: 28,
            maxWidth: 40
          }}>
            <FlatButton
              icon={<Cancel />}
              onClick={ props.deleteInstruction }
              disabled={ props.index >= props.length - 1 }
              fullWidth
              primary
              containerElement="label"
            />
          </div>
        </div>
      </li>
    )
  }
}
