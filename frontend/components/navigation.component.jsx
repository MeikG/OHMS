import { h, Component } from 'preact';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'preact-router/match';
import typography from './styles/typography.scss';

export default class extends Component {
  state = {
    open: false
  };

  showDrawer = () => this.setState({ open: true });
  hideDrawer = () => this.setState({ open: false });

  render() {
    return (
      <div>
        <AppBar title="Pocket Sous" onLeftIconButtonClick={ this.showDrawer } style={{ position: 'fixed' }}/>

        <Drawer
          docked={false}
          width={200}
          open={this.state.open}
          onRequestChange={open => this.setState({open})}
        >
          <div styleName="typography.title" style={{ margin: 16, color: '#555' }}>Pocket Sous</div>
          <MenuItem onClick={this.hideDrawer} containerElement={<Link href="/"/>}>Home</MenuItem>
          <MenuItem onClick={this.hideDrawer} containerElement={<Link href="/recipes"/>}>Recipes</MenuItem>
          <MenuItem onClick={this.hideDrawer} containerElement={<Link href="/schedule"/>}>Scheduling</MenuItem>
          <MenuItem onClick={this.hideDrawer} containerElement={<Link href="/meal-planner"/>}>Meal Planning</MenuItem>
          <MenuItem onClick={this.hideDrawer} containerElement={<Link href="/cool-along"/>}>Cook Along</MenuItem>
          <MenuItem onClick={this.hideDrawer} containerElement={<Link href="/about"/>}>About</MenuItem>
        </Drawer>
      </div>
    )
  }
}
