import { h } from 'preact';
import CircularProgress from 'material-ui/CircularProgress';
import Ingredient from './ingredient.component';
import Instruction from './instruction.component';
import {
  INGREDIENT_NAME, INGREDIENT_QUANTITY, INSTRUCTION_STEP,
  INSTRUCTION_TIMING
} from "../containers/addNewRecipe.container";

import layout from './styles/layout.scss';
import typography from './styles/typography.scss';
import flexbox from 'flexboxgrid/dist/flexboxgrid.css';

export default props => {
  if (props.loading) {
    return (
      <div styleName="layout.fullScreen">
        <CircularProgress size={70} thickness={5}/>
      </div>
    )
  } else {
    return (
      <div>
        <h1 styleName="typography.display2">{props.name}</h1>
        <strong>{props.source}</strong>
        <p>{props.description}</p>
        <em>Serves {props.servings}</em>

        <h2 styleName="typography.title">Ingredients</h2>
        <ul styleName="flexbox.row">
          {props.ingredients.map((ingredient, index) => {
            return (
              <div styleName="flexbox.col-xs-12 flexbox.col-md-6" key={index}>
                <Ingredient index={index}
                            length={props.ingredients.length}
                            quantity={ingredient[INGREDIENT_QUANTITY]}
                            name={ingredient[INGREDIENT_NAME]}
                />
              </div>
            )
          })}
        </ul>

        <h2 styleName="typography.title">Meal Prep</h2>
        <ul styleName="flexbox.row">
          { props.mealPrep.map((instruction, index, arr) => {
            return (
              <div styleName="flexbox.col-xs-12" key={index}>
                <Instruction index={ index }
                             uniqueName="mealPrep"
                             length={ arr.length }
                             timing={ instruction[INSTRUCTION_TIMING] }
                             step={ instruction[INSTRUCTION_STEP] }
                />
              </div>
            )
          })}
        </ul>

        <h2 styleName="typography.title">Instructions</h2>
        <ul styleName="flexbox.row">
          { props.instructions.map((instruction, index, arr) => {
            return (
              <div styleName="flexbox.col-xs-12" key={index}>
                <Instruction index={ index }
                             uniqueName="instructions"
                             length={ arr.length }
                             timing={ instruction[INSTRUCTION_TIMING] }
                             step={ instruction[INSTRUCTION_STEP] }
                />
              </div>
            )
          })}
        </ul>

      </div>
    )
  }
}
