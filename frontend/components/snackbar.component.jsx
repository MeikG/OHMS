import { h, Component } from 'preact';
import Snackbar from 'material-ui/Snackbar';

export default props => (
  <Snackbar
    open={ props.open }
    message={ props.message }
    autoHideDuration={ 3000 }
    onRequestClose={ props.dismissNotification }
  />
)
