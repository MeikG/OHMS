import { h, Component } from 'preact';
import CircularProgress from 'material-ui/CircularProgress';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Link } from 'preact-router/match';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

import typography from './styles/typography.scss';
import layout from './styles/layout.scss';
import recipes from './styles/recipes.scss';
import flexbox from 'flexboxgrid/dist/flexboxgrid.css';

export default class extends Component {
  /**
   * Component Lifecycle Event
   *
   * Requests the recipes when the component has mounted.
   */
  componentDidMount() {
    this.props.requestRecipes();
  }

  /**
   * Displays a loading screen while the recipes are loading.
   *
   * @returns {*}
   */
  displayRecipes() {
    if (this.props.recipes === null) {
      return (
        <div styleName="layout.fullScreen">
          <CircularProgress size={80} thickness={5} />
        </div>
      );
    } else if (this.props.recipes.length === 0) {
      return (
        <div styleName="layout.fullScreen typography.text-center">
          <div styleName="typography.title">You don't have any recipes, why not create some?</div>
        </div>
      );
    } else {
      return (
        <div styleName="flexbox.row">
          { this.props.recipes.map((recipe, index) => (
            <div styleName="flexbox.col-xs-12 flexbox.col-sm-6 flexbox.col-md-4">
              <Card styleName="recipes.card" key={ index } style={{ top: 0, left: 0 }}>
                <CardTitle styleName="typography.display4" titleStyle={{ color: 'inherit' }} title={ recipe.name }/>
                <CardText>{ recipe.description }</CardText>
                <CardActions styleName="recipes.buttons" style={{ position: 'absolute' }}>
                  <FlatButton primary label="Cook"/>
                  <FlatButton label="Recipe" href={ recipe.url }/>
                </CardActions>
              </Card>
            </div>
          ))}
        </div>
      )
    }
  }

  render() {
    return (
      <div>
        <h1 styleName="typography.display1">Recipes</h1>
        { this.displayRecipes() }
        <FloatingActionButton style={{ position: 'fixed', right: 16, bottom: 16, zIndex: 1 }} containerElement={<Link href="/recipes/add-new"/>}>
          <ContentAdd />
        </FloatingActionButton>
      </div>
    )
  }
}
