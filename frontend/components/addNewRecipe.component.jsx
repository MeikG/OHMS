import { h, Component } from 'preact';

import { ValidatorForm } from 'react-form-validator-core';
import { TextValidator } from 'react-material-ui-form-validator';
import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
} from 'material-ui/Stepper';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import AutoComplete from 'material-ui/AutoComplete';
import Paper from 'material-ui/Paper';
import PhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import CircularProgress from 'material-ui/CircularProgress';
import { uploadImage } from '../utilities';

import Ingredient from './ingredient.component';
import Instruction from './instruction.component';

import typography from './styles/typography.scss';
import layout from './styles/layout.scss';
import addNewRecipe from './styles/addNewRecipe.scss';
import flexbox from 'flexboxgrid/dist/flexboxgrid.css';

import {
  INSTRUCTION_STEP,
  INSTRUCTION_TIMING,
  INGREDIENT_QUANTITY,
  INGREDIENT_NAME
} from "../containers/addNewRecipe.container";

export default props => (
  <div>
    {
      props.isFetching && (
        <div styleName="layout.fullScreen" style={{ backgroundColor: 'rgba(245,245,245, 0.7)', position: 'fixed', zIndex: 10, paddingTop: 64 }}>
          <CircularProgress size={70} thickness={5} />
        </div>
      )
    }
    <h1 styleName="typography.display1">Add New Recipe</h1>
    <ValidatorForm action="#" onSubmit={ props.nextStep } instantValidate={ false } onError={ props.handleErrors }>
      <Stepper activeStep={props.stepIndex} orientation="vertical" style={{ margin: '0 -16px' }}>
        <Step>

          <StepLabel>Recipe Details</StepLabel>
          <div styleName="addNewRecipe.step addNewRecipe.step-open">
            <p styleName="typography.noBottomMargin">Please enter the details of the recipe:</p>
            <div styleName="flexbox.row">
              <div styleName="flexbox.col-xs-8 flexbox.col-md-10">
                <TextValidator
                  floatingLabelText="Name of Recipe*"
                  name="name"
                  value={ props.recipeName }
                  onChange={ props.updateRecipeName }
                  fullWidth
                  validators={["minStringLength: 1", "matchRegexp:^[A-Z0-9]", "minStringLength: 4"]}
                  errorMessages={['Recipes must have a name','Recipes must start with a capital letter', 'Recipe name must be at least 4 characters']}
                  autocomplete="off"
                />
              </div>

              <div styleName="flexbox.col-xs-4 flexbox.col-md-2">
                <TextValidator
                  floatingLabelText="Servings"
                  type="number"
                  min={1}
                  max={100}
                  fullWidth
                  name="servings"
                  value={ props.recipeServings }
                  onChange={ props.updateRecipeServings }
                  validators={["minStringLength: 1", "minNumber: 1", "maxNumber: 100"]}
                  errorMessages={['Recipes must have servings','Recipes must have at least 1 serving', 'Recipes cannot have more than 100 servings']}
                  autocomplete="off"
                />
              </div>
            </div>

            <TextValidator
              floatingLabelText="Recipe Description"
              name="description"
              value={ props.recipeDescription }
              onChange={ props.updateRecipeDescription }
              fullWidth
              multiLine
              rows={2}
              rowsMax={2}
              autocomplete="off"
            />

            <AutoComplete
              floatingLabelText="Source"
              name="source"
              hintText="Book/Website"
              searchText={ props.recipeSource }
              onFocus={ props.fetchSources }
              onUpdateInput={ props.updateRecipeSource }
              dataSource={ props.autoCompleteSources }
              fullWidth
              autocomplete="off"
            />
            { props.stepIndex === 0 && (
              <div>
                <em style={{ display: 'block', margin: '16px 0'}}>* required field</em>
                <RaisedButton
                  label='Next'
                  disableTouchRipple={true}
                  disableFocusRipple={true}
                  primary
                  type="submit"
                />
              </div>
            )}
          </div>
        </Step>

        <Step>
          <StepLabel>Ingredients</StepLabel>
          <div styleName={ `addNewRecipe.step ${ props.stepIndex >= 1 ? 'addNewRecipe.step-open' : '' }` }>
            <p>Please enter all of the ingredients.</p>
            { props.stepIndex >= 1 && (
              <ul styleName="flexbox.row addNewRecipe.ul">
                { props.ingredients.map((ingredient, index) => {
                  return (
                    <div styleName="flexbox.col-xs-12 flexbox.col-md-6" key={index}>
                      <Ingredient editing
                                  required
                                  index={ index }
                                  length={ props.ingredients.length }
                                  quantity={ ingredient[INGREDIENT_QUANTITY] }
                                  name={ ingredient[INGREDIENT_NAME] }
                                  autoCompleteIngredients={ props.autoCompleteIngredients }
                                  fetchIngredients={ props.fetchIngredients }
                                  updateQuantity={ event => props.updateIngredient(index, INGREDIENT_QUANTITY, event.target.value) }
                                  updateName={ value => props.updateIngredient(index, INGREDIENT_NAME, value) }
                                  deleteIngredient={ () => props.deleteIngredient(index) }
                      />
                    </div>
                  )
                })}
              </ul>
            )}
            { props.stepIndex === 1 && (
              <div>
                <em style={{ display: 'block', margin: '16px 0'}}>* required field</em>
                <RaisedButton
                  label='Next'
                  disableTouchRipple={true}
                  disableFocusRipple={true}
                  primary
                  type="submit"
                />
              </div>
            )}
          </div>
        </Step>

        <Step>
          <StepLabel>Meal Prep</StepLabel>
          <div styleName={ `addNewRecipe.step ${ props.stepIndex >= 2 ? 'addNewRecipe.step-open' : '' }` }>
            <p>Please enter all of the steps and timings required to prepare this meal (not including cooking).</p>
            { props.stepIndex >= 2 && (
              <ul styleName="flexbox.row addNewRecipe.ul">
                { props.mealPrep.map((instruction, index) => {
                  return (
                    <div styleName="flexbox.col-xs-12" key={index}>
                      <Instruction editing
                                   index={ index }
                                   uniqueName="mealPrep"
                                   length={ props.mealPrep.length }
                                   timing={ instruction[INSTRUCTION_TIMING] }
                                   step={ instruction[INSTRUCTION_STEP] }
                                   updateTiming={ event => props.updateMealPrep(index, INSTRUCTION_TIMING, event.target.value) }
                                   updateStep={ event => props.updateMealPrep(index, INSTRUCTION_STEP, event.target.value) }
                                   deleteInstruction={ () => props.deleteMealPrep(index) }
                      />
                    </div>
                  )
                })}
              </ul>
            )}
            { props.stepIndex === 2 && (
              <div>
                <em style={{ display: 'block', margin: '16px 0'}}>* required field</em>
                <RaisedButton
                  label='Next'
                  disableTouchRipple={true}
                  disableFocusRipple={true}
                  primary
                  type="submit"
                />
              </div>
            )}
          </div>
        </Step>

        <Step>
          <StepLabel>Instructions</StepLabel>
          <div styleName={ `addNewRecipe.step ${ props.stepIndex >= 3 ? 'addNewRecipe.step-open' : '' }` }>
            <p>Please enter the full cooking instructions, and approximate timings for each step.</p>
            { props.stepIndex >= 3 && (
              <ul styleName="flexbox.row addNewRecipe.ul">
                { props.instructions.map((instruction, index) => {
                  return (
                    <div styleName="flexbox.col-xs-12" key={index}>
                      <Instruction editing
                                   required
                                   uniqueName="instructions"
                                   index={ index }
                                   length={ props.instructions.length }
                                   timing={ instruction[INSTRUCTION_TIMING] }
                                   step={ instruction[INSTRUCTION_STEP] }
                                   updateTiming={ event => props.updateInstructions(index, INSTRUCTION_TIMING, event.target.value) }
                                   updateStep={ event => props.updateInstructions(index, INSTRUCTION_STEP, event.target.value) }
                                   deleteInstruction={ () => props.deleteInstructions(index) }
                      />
                    </div>
                  )
                })}
              </ul>
            )}
            { props.stepIndex === 3 && (
              <div>
                <em style={{ display: 'block', margin: '16px 0'}}>* required field</em>
                <RaisedButton
                  label='Next'
                  disableTouchRipple={true}
                  disableFocusRipple={true}
                  primary
                  type="submit"
                />
              </div>
            )}
          </div>
        </Step>

        <Step>
          <StepLabel>Save your recipe</StepLabel>
          <div styleName={ `addNewRecipe.step ${ props.stepIndex >= 4 ? 'addNewRecipe.step-open' : '' }` }>
            <p>
              Click Submit to save your recipe, and add it to Pocket Sous.
            </p>
            <RaisedButton
              label='Create Recipe'
              disableTouchRipple={true}
              disableFocusRipple={true}
              primary
              type="submit"
            />
          </div>
        </Step>
      </Stepper>
    </ValidatorForm>
  </div>
)
