/*
 * Action Types
 */

// Recipe Collection Actions
export const NEW_NOTIFICATION = 'new_notification';
export const CLOSE_NOTIFICATION = 'close_notification';
/*
 * Action Creators
 */

/**
 * Opens a new snackbar.
 *
 * @param {string} message
 * @returns {{type: string, payload: *}}
 */
export function newNotification(message) {
  return {
    type: NEW_NOTIFICATION,
    message
  }
}

/**
 * Dismisses the currently open snackbar.
 *
 * @returns {{type: string}}
 */
export function dismissNotification() {
  return {
    type: CLOSE_NOTIFICATION
  }
}
