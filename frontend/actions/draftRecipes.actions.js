import { fetch } from '../utilities';
import { receiveRecipe, invalidateRecipes } from "./recipes.actions";
import { FAILURE_OFFLINE,
  FAILURE_BAD_REQUEST,
  FAILURE_NOT_FOUND,
  FAILURE_SERVER_ERROR,
  FAILURE_FORBIDDEN
} from "../utilities/fetch.utility";

import { newNotification } from "./snackbar.actions";

/*
 * Action Types
 */
export const FETCHING_DRAFT_RECIPE = 'fetching_draft_recipe';
export const RESET_DRAFT_STATE = 'reset_draft_state';

export const FETCH_INGREDIENTS = 'draft_recipe_fetch_ingredients';
export const RECEIVE_INGREDIENTS = 'draft_recipe_receive_ingredients';
export const FAIL_INGREDIENTS = 'draft_recipe_fail_ingredients';

export const FETCH_SOURCES = 'draft_recipe_fetch_sources';
export const RECEIVE_SOURCES = 'draft_recipe_receive_sources';
export const FAIL_SOURCES = 'draft_recipe_fail_sources';

/*
 * Action Creators
 */

function isFetching(isFetching) {
  return {
    type: FETCHING_DRAFT_RECIPE,
    payload: isFetching
  }
}

export function resetDraftState(isReset) {
  return {
    type: RESET_DRAFT_STATE,
    payload: isReset
  }
}

function recipeCreateFailure(err) {
  return dispatch => {

    let message = '';
    switch (err) {
      default:
        message = 'An unknown error occurred';
        break;
      case FAILURE_OFFLINE:
        message = 'You are offline or the server is not reachable';
        break;
      case FAILURE_NOT_FOUND:
        message = 'The server could not be found';
        break;
      case FAILURE_BAD_REQUEST:
        message = 'The server rejected the request';
        break;
      case FAILURE_FORBIDDEN:
        message = 'You are not logged in';
        break;
      case FAILURE_SERVER_ERROR:
        message = 'A server error occurred';
        break;
    }

    dispatch(newNotification(message));
  }
}

/**
 * Sends the draft recipe to the API.
 *
 * @returns {function(*, *)}
 */
export function submitDraftRecipe(recipe) {
  return dispatch => {
    dispatch(isFetching(true));

    const { ingredients, servings, mealPrep, instructions } = recipe;

    // Strip empty ingredients, meal prep & instructions from the request.
    const body = {
      ...recipe,
      servings: parseInt(servings),
      ingredients: ingredients
        .filter(v => v.quantity.length > 0 && v.name.length > 0)
        .map((v, i) => { return {...v, order: i } }),
      mealPrep: mealPrep
        .filter(v => v.timing !== null || v.step.length > 0)
        .map((v, i) => v.timing ? { ...v, order: i } : {...v, timing: null, order: i }),
      instructions: instructions
        .filter(v => v.timing !== null || v.step.length > 0)
        .map((v, i) => v.timing ? { ...v, order: i } : {...v, timing: null, order: i })
    };

    // Submit the draft recipe to the API
    return fetch('/recipes', {
      method: 'POST',
      body,
    }).then(() => {
      // API request is over.
      dispatch(isFetching(false));

      // Invalidate the Recipes from the store.
      dispatch(invalidateRecipes());

      // Show a SnackBar to direct the user to the new recipe.
      dispatch(newNotification('Successfully created recipe'));

      // Trigger the draft state to reset.
      dispatch(resetDraftState(true));
    }).catch(err => {
      // API request is over.
      dispatch(isFetching(false));

      // Generate a snackbar notification about the error.
      dispatch(recipeCreateFailure(err));

      console.error(`draftRecipes submitDraft caught: ${ err }`)
    })
  }
}

function fetchIngredientsFromAPI() {
  return {
    type: FETCH_INGREDIENTS
  }
}

function receiveIngredientsFromAPI(payload) {
  return {
    type: RECEIVE_INGREDIENTS,
    receivedAt: Date.now(),
    payload
  }
}

function failToReceiveIngredientsFromAPI() {
  return {
    type: FAIL_INGREDIENTS
  }
}

/**
 * Fetch the ingredients from the API for autocomplete.
 *
 * @returns {function(*, *)}
 */
export function fetchIngredients() {
  return (dispatch, state) => {
    const { draftRecipes } = state();
    const { isFetching, receivedAt } = draftRecipes.ingredients;

    if (! isFetching && receivedAt === null) {

      // Mark the ingredients as receiving.
      dispatch(fetchIngredientsFromAPI());

      return fetch('/recipes/add-new/ingredients')
        .then(ingredients => {
          // Save the ingredients.
          dispatch(receiveIngredientsFromAPI(ingredients));
        }).catch(err => {
          console.error(err);

          // Reset the ingredients back to null.
          dispatch(failToReceiveIngredientsFromAPI());

          dispatch(newNotification('Could not receive ingredients'));
        });
    }
  }
}

function fetchSourcesFromAPI() {
  return {
    type: FETCH_SOURCES
  }
}

function receiveSourcesFromAPI(payload) {
  return {
    type: RECEIVE_SOURCES,
    receivedAt: Date.now(),
    payload
  }
}

function failToReceiveSourcesFromAPI() {
  return {
    type: FAIL_SOURCES
  }
}

/**
 * Fetch the sources from the API for auto-complete.
 *
 * @returns {function(*, *)}
 */
export function fetchSources() {
  return (dispatch, state) => {
    const { draftRecipes } = state();
    const { isFetching, receivedAt } = draftRecipes.sources;

    if (!isFetching && receivedAt === null) {

      // Mark the ingredients as receiving.
      dispatch(fetchSourcesFromAPI());

      return fetch('/recipes/add-new/sources')
        .then(sources => {
          // Save the ingredients.
          dispatch(receiveSourcesFromAPI(sources));
        }).catch(err => {
          console.error(err);

          // Reset the ingredients back to null.
          dispatch(failToReceiveSourcesFromAPI());

          dispatch(newNotification('Could not receive sources'));
        });
    }
  }
}
