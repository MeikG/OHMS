import { fetch } from '../utilities';

/*
 * Action Types
 */

// Recipe Collection Actions
export const REQUEST_RECIPES = 'request_recipes';
export const INVALIDATE_RECIPES = 'invalidate_recipes';
export const RECEIVE_RECIPES = 'receive_recipes';

// Individual Recipe Actions
export const REQUEST_INDIVIDUAL_RECIPE = 'request_individual_recipe';
export const INVALIDATE_INDIVIDUAL_RECIPE = 'invalidate_individual_recipe';
export const RECEIVE_INDIVIDUAL_RECIPE = 'receive_individual_recipe';
export const REJECTED_INDIVIDUAL_RECIPE = 'rejected_individual_recipe';

export const REQUEST_SIMPLIFIED_RECIPE = 'request_simplified_recipe';
export const RECEIVE_SIMPLIFIED_RECIPE = 'receive_simplified_recipe';

/*
 * Action Creators
 */

/**
 * Fetches all recipes from the recipes endpoint.
 *
 * @returns {{type: string, recipe: *}}
 */
function requestRecipes() {
  return {
    type: REQUEST_RECIPES
  }
}

/**
 * Fetches a specific recipe from the api.
 *
 * @returns {{type: string, recipe: *}}
 */
function requestRecipe(url) {
  return {
    type: REQUEST_INDIVIDUAL_RECIPE,
    url
  }
}

/**
 * Requests a simplified recipe from the collections.
 *
 * @returns {{type: string, recipe: *}}
 */
function requestSimplifiedRecipe(url) {
  return {
    type: REQUEST_SIMPLIFIED_RECIPE,
    url
  }
}

/**
 * Invalidates all of the recipes from the store.
 *
 * @returns {{type: string}}
 */
export function invalidateRecipes() {
  return {
    type: INVALIDATE_RECIPES
  }
}

/**
 * Invalidates a specific recipe.
 *
 * @param url
 * @returns {{type: string, url: *}}
 */
function invalidateRecipe(url) {
  return {
    type: INVALIDATE_INDIVIDUAL_RECIPE,
    url
  }
}

/**
 * Receives the recipes.
 *
 * @param recipes
 * @returns {{type: string, recipes: *, receivedAt: number}}
 */
function receiveRecipes(recipes) {
  return {
    type: RECEIVE_RECIPES,
    recipes,
    receivedAt: Date.now()
  }
}

/**
 * Receives a specific recipe.
 *
 * @param url
 * @param recipe
 * @returns {{type: string, url: *, recipe: *, receivedAt: number}}
 */
export function receiveRecipe(url, recipe) {
  return {
    type: RECEIVE_INDIVIDUAL_RECIPE,
    url,
    recipe,
    receivedAt: Date.now()
  }
}

/**
 * Receives a simplified version of the recipe from the collection.
 *
 * @param url
 * @param recipe
 * @returns {{type: string, url: *, recipe: *}}
 */
function receiveSimplifiedRecipe(url, recipe) {
  return {
    type: RECEIVE_SIMPLIFIED_RECIPE,
    url,
    recipe,
    receivedAt: Date.now()
  }
}

/**
 * Fetches recipes from the recipes endpoint.
 *
 * @returns {function(*, *)}
 */
export function fetchRecipes() {
  return (dispatch, state) => {
    if (staleRecipes(state())) {

      // If the recipes are considered stale, dispatch requestRecipes, and fetch it from the API.
      dispatch(requestRecipes());
      return fetch('/recipes')
        .then(json => {
          // If the response isn't an array, fail here.
          if (! Array.isArray(json)) throw json.error || 'Unspecified Error';

          // Dispatch the received recipes to the store.
          dispatch(receiveRecipes(json));
        }).catch(err => console.error(`fetchRecipes caught: ${ err }`))
    }
  }
}

/**
 * Fetches a specific recipe from the API.
 *
 * @param url
 * @returns {function(*, *)}
 */
export function fetchRecipe(url) {
  return (dispatch, state) => {
    // Only fetch if the recipe is stale.
    if (staleRecipe(url, state())) {
      // Dispatch a request for the simplified recipe, and the recipe from the endpoint itself.
      dispatch(requestRecipe(url));

      return fetch(`/recipes/${ url }`)
        .then(json => dispatch(receiveRecipe(url, json)))
        .catch(status => dispatch(rejectRecipe(url, status)))
    }
  }
}

/**
 * Determines whether or not the recipes are stale.
 *
 * @param state
 * @returns {boolean}
 */
function staleRecipes({ recipes }) {
  if (! recipes.recipeCollection || recipes.recipeCollection.length === 0) {
    // No recipes in collection.
    return true;
  } else if (recipes.isFetching !== false) {
    // The recipe is fetching, but if the fetch request has been active for more than 10 seconds, then re-request.
    return (Date.now() - recipes.isFetching > 10000);
  } else {
    return recipes.invalidated;
  }
}

/**
 * Determines if a specific recipe is stale or not.
 *
 * @param url
 * @param state
 * @returns {boolean}
 */
function staleRecipe(url, { recipes }) {
  const recipe = recipes.detailedRecipe[url];

  if (typeof recipe  === 'undefined') {
    // No recipe in store, so request.
    return true;
  } else if (recipe.isFetching !== false) {
    // The recipe is fetching, but if the data is older than 10 seconds then re-request.
    return (Date.now() - recipe.isFetching > 10000);
  } else {
    return recipe.invalidated;
  }
}

/**
 * Record that the recipe was rejected, and the HTTP status code received.
 *
 * @param url
 * @param status
 * @returns {{type: string, url: *, status: *, receivedAt: number}}
 */
function rejectRecipe(url, status) {
  return {
    type: REJECTED_INDIVIDUAL_RECIPE,
    url,
    status,
    receivedAt: Date.now()
  }
}

