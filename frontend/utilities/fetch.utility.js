export const FAILURE_OFFLINE = 0;
export const FAILURE_BAD_REQUEST = 400;
export const FAILURE_FORBIDDEN = 403;
export const FAILURE_NOT_FOUND = 404;
export const FAILURE_SERVER_ERROR = 500;

/**
 * Isomorphic Fetch wrapper.
 *
 * @param   {string}  url       URL endpoint
 * @param   {Object}  options   Fetch API options
 *
 * @returns {JSON}    JSON Response from server.
 * @throws  {int}     HTTP Status Code or 0 for offline.
 */
export default async (url, options = {}) => {
  // Inject our own options into the fetch request.
  const fetchOptions = {
    ...options,
    // If a body has been included, stringify it.
    body: JSON.stringify(options.body) || undefined,

    // Process and include the headers required to talk to the server.
    headers: new Headers({
      ...options.headers,
      // The server only accepts input that matches the client version.
      Version: process.env.OHMS_VERSION,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    })
  };

  return await fetch(url, fetchOptions)
    .then(response => {
      // If the response isn't okay, throw the status code.
      if (! response.ok) throw response.status;

      // Return the response's JSON.
      return response.json();
    }).catch(err => {
      // Throw a response status code if one was received, otherwise treat it as offline.
      throw Number.isInteger(err) ? err : 0;
    });
}
