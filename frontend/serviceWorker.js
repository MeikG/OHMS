import * as runtime from 'offline-plugin/runtime';

export default class ServiceWorker {
  constructor() {
    runtime.install({
      onUpdating: () => {
        console.log('SW Event:', 'onUpdating');
        let update = document.querySelector('.appContent__update');
        update.style.display = 'flex';
      },

      onUpdateReady: () => {
        console.log('SW Event:', 'onUpdateReady');
        // Tells to new SW to take control immediately
        runtime.applyUpdate();
      },

      onUpdated: () => {
        console.log('SW Event:', 'onUpdated');

        // Clear the local storage
        localStorage.clear();

        // Reload the webpage to load into the new version
        window.location.reload();
      },

      onUpdateFailed: () => {
        console.log('SW Event:', 'onUpdateFailed');
      }
    });
  }
}
