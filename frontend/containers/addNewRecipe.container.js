import { h, Component } from 'preact';
import { connect } from 'preact-redux'
import AddNewRecipe from "../components/addNewRecipe.component";
import { newNotification } from "../actions/snackbar.actions";
import linkState from 'linkstate';

import { submitDraftRecipe, resetDraftState, fetchIngredients, fetchSources } from "../actions/draftRecipes.actions";

const mapStateToProps = ({ draftRecipes }) => {
  return {
    isFetching: draftRecipes.isFetching,
    reset: draftRecipes.reset,
    autoCompleteIngredients: draftRecipes.ingredients.data,
    autoCompleteSources: draftRecipes.sources.data
  }
};

const mapDispatchToProps = dispatch => {
  return {
    submitDraftRecipe: state => dispatch(submitDraftRecipe(state)),
    newNotification: message => dispatch(newNotification(message)),
    resetDraftState: state => dispatch(resetDraftState(state)),
    fetchIngredients: () => dispatch(fetchIngredients()),
    fetchSources: () => dispatch(fetchSources())
  }
};

const RECIPE_NAME = 'name';
const RECIPE_SERVINGS = 'servings';
const RECIPE_DESCRIPTION = 'description';
const RECIPE_SOURCE = 'source';
const RECIPE_INGREDIENTS = 'ingredients';
const RECIPE_MEALPREP = 'mealPrep';
const RECIPE_INSTRUCTIONS = 'instructions';

export const INGREDIENT_QUANTITY = 'quantity';
export const INGREDIENT_NAME = 'name';
export const INSTRUCTION_TIMING = 'timing';
export const INSTRUCTION_STEP = 'step';

class Container extends Component {
  /**
   * Blank ingredient & instructions.
   *
   * @type { object }
   */
  blank = {
    [RECIPE_INGREDIENTS]: {
      [INGREDIENT_QUANTITY]: '',
      [INGREDIENT_NAME]: ''
    },
    [RECIPE_MEALPREP]: {
      [INSTRUCTION_TIMING]: null,
      [INSTRUCTION_STEP]: ''
    },
    [RECIPE_INSTRUCTIONS]: {
      [INSTRUCTION_TIMING]: null,
      [INSTRUCTION_STEP]: ''
    }
  };

  state = {
    recipe: {
      [RECIPE_NAME]: '',
      [RECIPE_SERVINGS]: '',
      [RECIPE_DESCRIPTION]: '',
      [RECIPE_SOURCE]: '',
      [RECIPE_INGREDIENTS]: [this.blank[RECIPE_INGREDIENTS]],
      [RECIPE_MEALPREP]: [this.blank[RECIPE_MEALPREP]],
      [RECIPE_INSTRUCTIONS]: [this.blank[RECIPE_INSTRUCTIONS]]
    },
    stepIndex: 0
  };

  // Clone the state for when it gets reset.
  defaultState = {
    ...this.state
  };

  /**
   * Handle resetting the state when requested to do so by Redux.
   *
   * @param { boolean } reset
   */
  componentWillReceiveProps({ reset }) {
    if (reset) {
      // Reset the state.
      this.setState(this.defaultState);

      // Return the reset trigger back to false.
      this.props.resetDraftState(false);
    }
  }

  /**
   * Increment the stepIndex counter in state, or submit to the API.
   */
  nextStep = () => {
    const { stepIndex } = this.state;

    if (stepIndex < 4) {
      // Increment the counter until we're at the final page.
      this.setState({
        stepIndex: stepIndex + 1,
      });
    } else {
      // Send the draft to the API.
      this.props.submitDraftRecipe(this.state.recipe);
    }
  };

  /**
   * Update a recipe field.
   *
   * @param { string|number } value
   * @param { string }        field   Field to update
   */
  updateRecipeField = (value, field) => {
    const { recipe } = this.state;
    this.setState({
      recipe: {
        ...recipe,
        [field]: value
      }
    });
  };

  /**
   * Update an entry within an array in the recipes.
   *
   * @param { string }  property    Array within recipes object.
   * @param { int }     index       Index of item within array.
   * @param { string }  key         Property within array to be updated.
   * @param { string }  value       Value to be set to.
   */
  updateEntry = (property, index, key, value) => {
    const { recipe } = this.state;

    // Clone the ingredients array without references.
    const data = [
      ...recipe[property]
    ];

    // If it's the last entry, then automatically add a new empty ingredient.
    if (index + 1 === data.length && typeof this.blank[property] !== 'undefined') {
      data.push(this.blank[property]);
    }

    // Update the state.
    this.setState({
      recipe: {
        ...recipe,
        [property]: data.map((v, i) => {
          if (i === index) {
            return {
              ...v,
              [key]: value
            }
          } else {
            return v;
          }
        })
      }
    });
  };

  /**
   * Remove an entry from an array within the recipes.
   *
   * @param { string }  entry
   * @param { int }     index
   */
  deleteEntry = (entry, index) => {
    const { recipe } = this.state;

    this.setState({
      recipe: {
        ...recipe,
        [entry]: recipe[entry].filter((ingredient, i) => i !== index)
      }
    });
  };

  /**
   * Handle what happens when the form fails validation.
   */
  handleErrors = () => {
    this.props.newNotification('Please check fields in red.');
  };

  render() {
    return (
      <AddNewRecipe
        isFetching={ this.props.isFetching }

        // Auto-complete props.
        autoCompleteSources={ this.props.autoCompleteSources }
        fetchSources={ this.props.fetchSources }

        autoCompleteIngredients={ this.props.autoCompleteIngredients }
        fetchIngredients={ this.props.fetchIngredients }

        // Pass down the current step index, and method for incrementing the step counter.
        stepIndex={ this.state.stepIndex }
        nextStep={ this.nextStep }

        // Handle what happens when form validation fails.
        handleErrors={ this.handleErrors }

        // Update basic recipe information.
        recipeName={ this.state.recipe[RECIPE_NAME] }
        recipeServings={ this.state.recipe[RECIPE_SERVINGS] }
        recipeDescription={ this.state.recipe[RECIPE_DESCRIPTION] }
        recipeSource={ this.state.recipe[RECIPE_SOURCE] }
        updateRecipeName={ event => this.updateRecipeField(event.target.value, RECIPE_NAME) }
        updateRecipeServings={ event => this.updateRecipeField(event.target.value, RECIPE_SERVINGS) }
        updateRecipeDescription={ event => this.updateRecipeField(event.target.value, RECIPE_DESCRIPTION) }
        updateRecipeSource={ value => this.updateRecipeField(value, RECIPE_SOURCE) }

        // Ingredients.
        ingredients={ this.state.recipe[RECIPE_INGREDIENTS] }
        updateIngredient={ (index, key, value) => this.updateEntry(RECIPE_INGREDIENTS, index, key, value) }
        deleteIngredient={ index => this.deleteEntry(RECIPE_INGREDIENTS, index) }

        // Meal Prep.
        mealPrep={ this.state.recipe[RECIPE_MEALPREP] }
        updateMealPrep={ (index, key, value) => this.updateEntry(RECIPE_MEALPREP, index, key, value) }
        deleteMealPrep={ index => this.deleteEntry(RECIPE_MEALPREP, index) }

        // Cooking Instructions
        instructions={ this.state.recipe[RECIPE_INSTRUCTIONS] }
        updateInstructions={ (index, key, value) => this.updateEntry(RECIPE_INSTRUCTIONS, index, key, value) }
        deleteInstructions={ index => this.deleteEntry(RECIPE_INSTRUCTIONS, index) }
      />
      )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
