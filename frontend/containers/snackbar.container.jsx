import { connect } from 'preact-redux'
import component from "../components/snackbar.component";
import { dismissNotification } from "../actions/snackbar.actions";

const mapStateToProps = ({ snackbar }) => {
  return {
    open: snackbar.open,
    message: snackbar.message
  }
};

const mapDispatchToProps = dispatch => {
  return {
    dismissNotification: () => dispatch(dismissNotification())
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
