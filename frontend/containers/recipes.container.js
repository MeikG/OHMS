import { connect } from 'preact-redux'
import component from '../components/recipes.component';
import { fetchRecipes } from "../actions/recipes.actions";

const mapStateToProps = ({ recipes, draftRecipes }) => {
  return {
    draftRecipes,
    recipes: recipes.recipeCollection || null
  }
};

const mapDispatchToProps = dispatch => {
  return {
    requestRecipes: () => dispatch(fetchRecipes())
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
