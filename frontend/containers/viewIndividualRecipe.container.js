import { h, Component } from 'preact';
import ViewIndividualRecipe from '../components/viewIndividualRecipe.component';
import { fetchRecipe } from '../actions/recipes.actions'
import { connect } from 'preact-redux'

const mapStateToProps = ({ recipes }) => {
  const { detailedRecipe } = recipes;

  return {
    detailedRecipe
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchRecipe: (url) => dispatch(fetchRecipe(url))
  }
};

class Container extends Component {
  componentDidMount() {
    const { recipe } = this.props;
    this.props.fetchRecipe(recipe);
  }

  render() {
    const { recipe, detailedRecipe } = this.props;
    const currentRecipe = detailedRecipe[recipe];

    if (typeof currentRecipe === 'undefined' || currentRecipe.state !== 'recipe_state_full') {
      return (
        <ViewIndividualRecipe
          loading
        />
      )
    } else {
      const { data } = currentRecipe;

      return (
        <ViewIndividualRecipe
          name={ data.name }
          servings={ data.servings }
          description={ data.description }
          source={ data.source }
          ingredients={ data.ingredients }
          mealPrep={ data.mealPrep }
          instructions={ data.instructions }
        />
      )
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
