import 'babel-polyfill/dist/polyfill.min';
import {h, render} from 'preact';
import { Provider } from 'preact-redux';
import Navigation from './components/navigation.component';
import Router from './components/router.component';
import Snackbar from './containers/snackbar.container'
import ServiceWorker from './serviceWorker';
import configureStore from './configureStore';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {teal500} from 'material-ui/styles/colors';

import 'typeface-roboto';
import style from './components/styles/app.scss';

new ServiceWorker();
const store = configureStore();

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: teal500
  }
});


// Perform these actions after the page has fully loaded.
window.addEventListener('load', () => {
// Remove the preload content from the DOM.
  let preload = document.querySelector('.appContent__loading');
  preload.style.display = 'none';

  render(
    <Provider store={ store }>
      <MuiThemeProvider muiTheme={muiTheme}>
        <div styleName='style.container'>
          <Navigation/>
          <div styleName='style.content'>
            <Router/>
          </div>
          <Snackbar/>
        </div>
      </MuiThemeProvider>
    </Provider>
    , document.body);
});
