import {
  FETCHING_DRAFT_RECIPE,
  RESET_DRAFT_STATE,
  FETCH_INGREDIENTS,
  RECEIVE_INGREDIENTS,
  FAIL_INGREDIENTS,
  FETCH_SOURCES,
  RECEIVE_SOURCES,
  FAIL_SOURCES
} from "../actions/draftRecipes.actions";

const defaultState = {
  isFetching: false,
  reset: false,
  ingredients: {
    isFetching: false,
    receivedAt: null,
    data: []
  },
  sources: {
    isFetching: false,
    receivedAt: null,
    data: []
  }
};

export function draftRecipes(state = { ...defaultState }, action) {
  switch (action.type) {
    case FETCHING_DRAFT_RECIPE:
      return {
        ...state,
        isFetching: action.payload
      };

    case RESET_DRAFT_STATE:
      if (action.payload) {
        // If true, reset the state back to empty.
        return {
          ...state,
          reset: true,
          ingredients: {
            ...defaultState.ingredients
          },
          sources: {
            ...defaultState.sources
          }
        }
      } else {
        return {
          ...state,
          reset: false
        };
      }

    case FETCH_INGREDIENTS:
    return {
      ...state,
      ingredients: {
        ...state.ingredients,
        isFetching: true
      }
    };

    case RECEIVE_INGREDIENTS:
      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          isFetching: false,
          receivedAt: action.receivedAt,
          data: action.payload
        }
      };

    case FAIL_INGREDIENTS:
      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          isFetching: false,
          receivedAt: null
        }
      };

    case FETCH_SOURCES:
      return {
        ...state,
        sources: {
          ...state.sources,
          isFetching: true
        }
      };

    case RECEIVE_SOURCES:
      return {
        ...state,
        sources: {
          ...state.sources,
          isFetching: false,
          receivedAt: action.receivedAt,
          data: action.payload
        }
      };

    case FAIL_SOURCES:
      return {
        ...state,
        sources: {
          ...state.sources,
          isFetching: false,
          receivedAt: null
        }
      };

    default:
      return state;
  }
}
