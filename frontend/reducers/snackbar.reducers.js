import { NEW_NOTIFICATION, SET_OPEN, CLOSE_NOTIFICATION } from "../actions/snackbar.actions";

export function snackbar(state = { message: '', open: false }, action) {
  switch (action.type) {
    case NEW_NOTIFICATION:
      return {
        ...state,
        open: true,
        message: action.message,
      };
    case CLOSE_NOTIFICATION:
      return {
        ...state,
        open: false,
      };
    default:
      return state;
  }
}
