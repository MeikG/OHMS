import { REQUEST_RECIPES,
  INVALIDATE_RECIPES,
  RECEIVE_RECIPES,
  REQUEST_INDIVIDUAL_RECIPE,
  INVALIDATE_INDIVIDUAL_RECIPE,
  RECEIVE_INDIVIDUAL_RECIPE,
  REJECTED_INDIVIDUAL_RECIPE,
  RECEIVE_SIMPLIFIED_RECIPE,
} from "../actions/recipes.actions";

export const RECIPE_STATE_NONE = 'recipe_state_none';
export const RECIPE_STATE_SIMPLIFIED = 'recipe_state_simplified';
export const RECIPE_STATE_FULL = 'recipe_state_full';
export const RECIPE_STATE_FAILURE = 'recipe_state_failure';

export function recipes(
  state = {
    isFetching: false,
    invalidated: false,
    recipeCollection: null,
    detailedRecipe: {}
  }, action) {
  switch (action.type) {

    case REQUEST_RECIPES:
      return {
        ...state,
        isFetching: Date.now(),
        invalidated: false,
      };

    case INVALIDATE_RECIPES:
      return {
        ...state,
        invalidated: true
      };

    case RECEIVE_RECIPES:
      return {
        ...state,
        isFetching: false,
        invalidated: false,
        recipeCollection: action.recipes,
        lastUpdated: action.receivedAt
      };

    case REQUEST_INDIVIDUAL_RECIPE:
      // Mark the recipe as currently fetching.
      return {
        ...state,
        detailedRecipe: {
          ...state.detailedRecipe,
          [action.url]: {
            ...state.detailedRecipe[action.url],
            isFetching: Date.now(),
            invalidated: false,
            state: RECIPE_STATE_NONE,
          },
        }
      };

    case INVALIDATE_INDIVIDUAL_RECIPE:
      // Invalidate the recipe to be re-fetched.
      return {
        ...state,
        detailedRecipe: {
          [action.url]: {
            ...state.detailedRecipe[action.url],
            invalidated: true,
            state: RECIPE_STATE_NONE,
          },
        }
      };

    case RECEIVE_INDIVIDUAL_RECIPE:
      // Update the current recipe to no longer be fetching, and include the data from the API.
      return {
        ...state,
        detailedRecipe: {
          ...state.detailedRecipe,
          [action.url]: {
            ...state.detailedRecipe[action.url],
            isFetching: false,
            invalidated: false,
            data: action.recipe,
            state: RECIPE_STATE_FULL,
            lastUpdated: action.receivedAt
          },
        }
      };

    case REJECTED_INDIVIDUAL_RECIPE:
      // The currently requested recipe was rejected by the server.
      return {
        ...state,
        detailedRecipe: {
          ...state.detailedRecipe,
          [action.url]: {
            ...state.detailedRecipe[action.url],
            isFetching: false,
            invalidated: true,
            state: RECIPE_STATE_FAILURE,
            isRejected: action.status,
          }
        }
      };

    case RECEIVE_SIMPLIFIED_RECIPE:
      // Only proceed if the data is empty, otherwise data has already been received.
      if (state.detailedRecipe[action.url].state !== RECIPE_STATE_FULL) {
        return {
          ...state,
          detailedRecipe: {
            ...state.detailedRecipe,
            [action.url]: {
              ...state.detailedRecipe[action.url],
              data: action.recipe,
              state: RECIPE_STATE_SIMPLIFIED,
              lastUpdated: action.receivedAt
            }
          }
        };
      } else {
        // Don't replace detailed data with simplified.
        return state;
      }
    default:
      return state;
  }
}
