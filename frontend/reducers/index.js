import { combineReducers } from 'redux';
import { draftRecipes } from './draftRecipes.reducers'
import { recipes } from './recipes.reducers'
import { snackbar } from './snackbar.reducers'

// Combine the reducers.
const reducers = combineReducers({
  draftRecipes,
  recipes,
  snackbar
});

export default reducers;
