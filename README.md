# PocketSous
[https://pocketsous.mgregory.me](https://pocketsous.mgregory.me)

The sous chef in your pocket! PocketSous is a website designed to make your
cooking easier by managing your recipes and scheduling meals. 

PocketSous is currently in active development.

## Using PocketSous
### Hosted at pocketsous.mgregory.me
PocketSous is currently hosted on my server as a PaaS, allowing users a central
location to store and manage recipes and schedules. 

### Self-hosted
Requirements:
* Docker

PocketSous is based on the Docker, and Docker Compose infrastructure. To get a
fully functional infrastructure, execute the following commands:

```
git pull git@gitlab.com:MeikG/OHMS.git
cd OHMS
docker-compose up -d
```

This will configure all of the services required for PocketSous.